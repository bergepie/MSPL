## Projet: Pierre Berger & Axel Vincent

Notre travail porte sur l'étude des vins dans le monde et leur comparaison par pays. De ce fait nous allons travailler sur un jeu de données issu du site www.kaggle.com qui référencie 150 930 observations sur les vins, en tenant compte de leur note, de leur prix ainsi que de leur localité. 

Source data: https://www.kaggle.com/zynicide/wine-reviews


### Question 1 : Pouvons nous dire qu'il y a une correlation significative entre le prix et la note d'un vin? 

Aspect géographique sur cette correlation

Graph 1 : Lineaire\

Il n'y est pas clair qu'il y est une dépendance entre la note et le prix du vin.
